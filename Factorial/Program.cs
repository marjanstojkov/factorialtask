﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorial
{
    class Program
    {
        static void Main()
        {
            int number = 0;
            string input = "";

            do  
            {
                Console.Write("Please enter integer number (0<n<17) n=");
                input = Console.ReadLine();// user input
                try
                {
                    number = int.Parse(input);
                    
                } catch
                {
                    number = 0;
                } finally
                {
                    if ((number < 1) || (number > 16))
                    {
                        Console.WriteLine("\n*** Invalid input! ***\n");
                    }
                }
                          
            } while ((number <1)||(number>16));


            Console.WriteLine("{0}! = {1}\n",number,CalculateFactorial(number));// get and display result


            Console.Write("Press any key ...");
            Console.ReadLine();

        }

        static int CalculateFactorial(int n)
        {
            if (n == 1)
            {
                return 1;
            }
            else
              return n * CalculateFactorial(n - 1);
        }
    }
}
